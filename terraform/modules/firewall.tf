#Firewall rule to allow google health check to query the instances
resource "google_compute_firewall" "allow-healthcheck" {
  allow {
    ports    = [
      "80",
      "8080",
      "443"
      ]
    protocol = "tcp"
  }
   allow {
    protocol = "icmp"
  }

  description   = "allow-healthcheck"
  direction     = "INGRESS"
  disabled      = "false"
  name          = "allow-healthcheck"
  network       = google_compute_network.vpc_network.id
  priority      = "1000"
  project       = var.project_id
  source_ranges = [
    "130.211.0.0/22",
    "35.191.0.0/16",
    "209.85.152.0/22",
    "209.85.204.0/22"
    ]

    target_tags = ["dev"]
}

resource "google_compute_firewall" "allow-internal-ssh" {
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
   allow {
    protocol = "icmp"
  }

  description   = "allow internal ssh"
  direction     = "INGRESS"
  disabled      = "false"
  name          = "allow-internal-ssh"
  network       = google_compute_network.vpc_network.id
  priority      = "1000"
  project       = var.project_id
  source_ranges = ["10.5.0.0/20"]
  target_tags = ["dev"]
}

resource "google_compute_firewall" "allow-http-https" {
  allow {
    ports    = [
      "80",
      "8080",
      "443"
      ]
    protocol = "tcp"
  }
   allow {
    protocol = "icmp"
  }

  description   = "allow http(s)"
  direction     = "INGRESS"
  disabled      = "false"
  name          = "allow-http-https"
  network       = google_compute_network.vpc_network.id
  priority      = "1000"
  project       = var.project_id
  source_ranges = ["0.0.0.0/0"]
  target_tags = []
}

# ASK FERRIS - resource "google_compute_firewall" "allow-internal" {
#   allow {
#     ports    = ["0-65535"]
#     protocol = "tcp"
#   }
#    allow {
#     protocol = "icmp"
#   }

#   description   = "allow internal ssh"
#   direction     = "INGRESS"
#   disabled      = "false"
#   name          = "allow-internal-ssh"
#   network       = google_compute_network.network.id
#   priority      = "1000"
#   project       = var.project
#   source_ranges = ["10.5.0.0/20"]
#   target_tags = ["dev"]
# }
