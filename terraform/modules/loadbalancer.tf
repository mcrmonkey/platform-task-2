# Backend service for use with loadbalancer

resource "google_compute_backend_service" "platform-task-2-backend" {
  name                            = "platform-task-2-backend-service"
  connection_draining_timeout_sec = 0
  health_checks                   = [google_compute_health_check.autohealing.id]
  load_balancing_scheme           = "EXTERNAL_MANAGED"
  port_name                       = "http"
  protocol                        = "HTTP"
  session_affinity                = "NONE"
  timeout_sec                     = 30
  backend {
    group           = google_compute_region_instance_group_manager.platform-task-2-app.instance_group
    balancing_mode  = "UTILIZATION"
    capacity_scaler = 1.0
  }
}

resource "google_compute_global_address" "external-ip" {
  name       = "external-ip"
  ip_version = "IPV4"
}

resource "google_compute_url_map" "platform-task-2-map" {
  name            = "platform-task-2-map"
  default_service = google_compute_backend_service.platform-task-2-backend.id

  host_rule {
    hosts        = ["wl-louzado.com"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = google_compute_backend_service.platform-task-2-backend.id

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_service.platform-task-2-backend.id
    }

  }
}

resource "google_compute_target_http_proxy" "platform-task-2-lb-proxy" {
  name    = "platform-task-2-lb-proxy"
  url_map = google_compute_url_map.platform-task-2-map.id
}

resource "google_compute_global_forwarding_rule" "platform-task-2-fw" {
  name                  = "http-content-rule"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_range            = "80-80"
  target                = google_compute_target_http_proxy.platform-task-2-lb-proxy.id
  ip_address            = google_compute_global_address.external-ip.id
}

resource "google_compute_managed_ssl_certificate" "platform-task-2-ssl" {
  name    = "platform-task-2-ssl"

  managed {
    domains = [var.domain_name]
  }
}

resource "google_compute_target_https_proxy" "platform-task-2-test-proxy" {
  name             = "platform-task-2-proxy"
  url_map          = google_compute_url_map.platform-task-2-map.id
  ssl_certificates = [google_compute_managed_ssl_certificate.platform-task-2-ssl.id]
}