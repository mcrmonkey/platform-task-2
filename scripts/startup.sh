#! /bin/bash
NAME=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/name)
ZONE=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/instance/zone | sed 's@.*/@@')
sudo apt-get update
sudo apt-get install -y stress apache2
sudo a2ensite default-ssl
sudo a2enmod ssl
sudo systemctl start apache2
cat <<EOF> /var/www/html/index.html
<body style="font-family: sans-serif">
<html><body><h1>Hi, My name is Will and this is my Web Server!</h1>
<p>This machine is $NAME</p>
<p>Hosted in the $ZONE datacenter.</p>
</body></html>
EOF