#Change default values for your own

variable "project_id" {
  type        = string
  description = "Google Cloud Project"
  default     = "platform-task-2"
}

variable "env" {
  type        = string
  description = "environment name"
  default     = "dev"
}

variable "region" {
  type        = string
  description = "target region"
  default     = "us-east1"
}

variable "zones" {
  type        = list(string)
  description = "target zones"
  default     = ["us-east1-b", "us-east1-c", "us-east1-d"]
}

#Network variables

variable "network_ip_range_private" {
  type        = string
  description = "private network range for custom vpc"
  default     = "10.5.0.0/20"
}

variable "network_ip_range_public" {
  type        = string
  description = "private network range for custom vpc"
  default     = "10.0.0.0/20"
}

#Compute variables

variable "machine_type" {
  description = "machine type for mig instances"
  type        =  string
  default     = "f1-micro"
}

variable "web_image" {
  description = "image for the mig instances"
  type        = string
  default     = "web-debian-11"
}

variable "mig_target_size" {
  description = "minimum number of instances to start"
  type        = number
  default     = 3
}

variable "useradmin" {
  description = "permission for account for service account"
  type        = string
  default     = "will.louzado.gcp2@gmail.com"
}


variable "domain_name" {
    description = "primary domain for site"
    type        = string
    default     = "wl-louzado.com"
}

variable "serviceaccount" {
    description = "service account for compute instance"
    type        = string
    default     = "terraform@platform-task-2.iam.gserviceaccount.com"
}

variable "api_enable" {
  type        = set(string)
  description = "Project serices to enable"
  default = [
    "compute.googleapis.com",
    "iam.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "dns.googleapis.com",
    "servicenetworking.googleapis.com",
  ]
}

variable "labels" {
  description = "A map of labels to apply to contained resources."
  default     = {}
  type        = "map"
}