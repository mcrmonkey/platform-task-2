resource "google_compute_network" "vpc_network" {
  project                 = var.project_id
  name                    = "task-2-vpc-network"
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "private" {
  name                     = "${var.env}-private-network"
  ip_cidr_range            = var.network_ip_range_private
  network                  = google_compute_network.vpc_network.name
  region                   = var.region
  private_ip_google_access = true
}

resource "google_compute_subnetwork" "public" {
  name                     = "${var.env}-public-network"
  ip_cidr_range            = var.network_ip_range_public
  network                  = google_compute_network.vpc_network.name
  region                   = var.region
  private_ip_google_access = false
}

