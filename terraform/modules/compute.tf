#Create the instance template

resource "google_compute_instance_template" "platform-task-2-app-template" {
  name           = "platform-task-2-app-template"
  disk {
    auto_delete  = true
    boot         = true
    device_name  = "persistent-disk-0"
    mode         = "READ_WRITE"
    source_image = "projects/debian-cloud/global/images/family/debian-11"
    type         = "PERSISTENT"
  }
 labels = {
    env = "dev"
  }
  machine_type = var.machine_type
  metadata = {
    startup-script = file("startup.sh")
  }
  network_interface {
      subnetwork     = google_compute_subnetwork.public.self_link
  }
  region = var.region

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
    provisioning_model  = "STANDARD"
  }
  service_account {
    email  = var.serviceaccount #Compute service account not the terraform one
    scopes = ["cloud-platform"]
  }
  tags = ["dev"]
}

#Create Health check
resource "google_compute_health_check" "autohealing" {
  name                = "platform-task-2-health-check"
  description         = "Health check of managed instance group"
  check_interval_sec  = 5
  timeout_sec         = 5
  healthy_threshold   = 2
  unhealthy_threshold = 2
  project             = var.project_id

  log_config {
    enable = "false"
  }
  tcp_health_check {
    port = "80"
  }
}

#Create the Managed Instance Group

resource "google_compute_region_instance_group_manager" "platform-task-2-app" {
  name                      = "platform-task-2-app"
  base_instance_name        = "platform-task-2"
  distribution_policy_zones = var.zones
 
  named_port {
    name = "http"
    port = 80
  }
  version {
    instance_template = google_compute_instance_template.platform-task-2-app-template.self_link
  }
    auto_healing_policies {
    health_check      = google_compute_health_check.autohealing.id
    initial_delay_sec = 300
  }
  
  target_size        = var.mig_target_size
}

resource "google_compute_region_autoscaler" "autoscaler" {
  name   = "autoscaler"
  region = var.region
  target = google_compute_region_instance_group_manager.platform-task-2-app.id

  autoscaling_policy {
    max_replicas    = 6
    min_replicas    = 3
    cooldown_period = 60

    cpu_utilization {
      target = 0.6
    }
  }
}
