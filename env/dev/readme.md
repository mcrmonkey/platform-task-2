<div id="header" align="center">
  <img src="https://www.freecodecamp.org/news/content/images/2020/10/gcp.png" width="400"/>
</div>

# **Task 2**

> Create and configure a web server on GCP.
> Stretch goal - make it scalable!

> The purpose of this task was to repeat the above, but with Appsbroker and Google Cloud best practice in mind. I took the extra step of deploying the entire configuration using terraform. Its 90% of the way there with some remaining adjustments to the code for the front end config for the https load balancer.

> The intention is to create a proof of concept https load balancer with a managed instance group, built with terraform modules and following as closely to Google Cloud best practice as possible.

Target architecture is as below

![target-architecture](images/target-architecture.png)

## Pre-requisites

- Google Cloud Account
- Google Cloud Project set up
- Google Cloud Storage Bucket set up with adjustments made in backend.tf to store the statefile
- Relevent changes to the variables.tf to account for user unique values
- -TO DO

## How to run

TODO

## Best practice

TODO

## Things left to do

Instructions

Best practice comments

Terraform Front-end configuration to allow HTTPS load balancer with ssl certificate (needs manual configuration after spin up)

Terraform configuration file calling the built modules in a main.tf

Service account creations for compute instance, terraform account creation for building, removal of keys, use impersonation

Testing strategy - Kitchen

Bake virtual machine image using packer

google_*_iam_member for iam

Encrypt state
Though Google Cloud buckets are encrypted at rest, you can use customer-supplied encryption keys to provide an added layer of protection. Do this by using the GOOGLE_ENCRYPTION_KEY environment variable. Even though no secrets should be in the state file, always encrypt the state as an additional measure of defense.