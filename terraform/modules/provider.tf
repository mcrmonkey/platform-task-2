terraform {
    required_version = "~> 1.3.1"
}

provider "google" {
    version      = "~> 4.39.0"    
    credentials  = file("platform-task-2-35d550f3a5ad.json")
    project      = var.project_id
    region       = var.region 
}



